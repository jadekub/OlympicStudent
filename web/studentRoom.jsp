<%-- 
    Document   : secondpage
    Created on : 28-Apr-2017, 00:54:51
    Author     : User
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <style>
            td.aright{
                text-align: right;
            }
        </style>
    </head>
    <body bgcolor="708090">
    <center>
        <h1>โครงการส่งเสริมวิชาโอลิมปิกวิชาการ</h1>
    </center>
    
    <table cellpadding="2" width="" align="center" cellspacing="20">
        <tr>
            <td class="aright">
                เลขบัตรประชาชนผู้เข้าพัก :
            </td>
            <td><input type=text name=textids id="textid" size="13"></td>
        </tr>
        <tr>
            <td class="aright">
                เลขบัตรประชาชนผู้เข้าพักร่วม :
            </td>
            <td><input type=text name=textidmates id="textidmate" size="13"></td>
        </tr>
        <tr>
            <td class="aright">
                ชื่ออาคาร :
            </td>
            <td><select name="nametitle">
                    <option value="D1">อาคาร D1</option>
                    <option value="D2">อาคาร D2</option>
                    <option value="D3">อาคาร D3</option>
                    <option value="D4">อาคาร D4</option>
            </select></td>
        </tr>
        
        <tr>
            <td class="aright">
                เลขที่ห้อง :
            </td>
            <td><select name="nametitle">
                    <option value="room1201">ห้อง 1201</option>
                    <option value="room1202">ห้อง 1202</option>
            </select></td>
        </tr>
        <tr>
            <td class="aright">
                หมายเหตุ :
            </td>
            <td><input type=text name=textidmates id="textidmate" size="13"></td>
        </tr>
        <tr>
            <td class="aright">
                สถานะ :</td>
            <td>
                <form>
                    <input type="radio" name="status" value="sure">เป็นทางการ<br>
                    <input type="radio" name="status" value="notsure">ไม่เป็นทางการ<br>
                </form>
            </td>
        </tr>
    </table>
    
    <center>
        <button type="button">บันทึก</button>
    </center>
    <hr width="100%" size="1" color="black">
    
    <p>จำนวนผู้พักรวม :</p>
    
    <table align="center" border="2">
        <tr bgcolor="8470FF">
            <th>       </th>
            <th>เลขที่บัตรประชาชน</th>
            <th>ชื่อ-นามสกุล</th>
            <th>เพศ</th>
            <th>บัตรประชาชนผู้พักร่วม</th>
            <th>ชื่อ-นามสกุลผู้พักร่วม</th>
            <th>ชื่ออาคาร</th>
            <th>รหัสห้อง</th>
            <th>หมายเหตุ</th>
            <th>สถานะ</th>
            <th>       </th>  
        </tr>
        
    </table>
    </body>
</html>
